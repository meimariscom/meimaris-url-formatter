#Reads URL's from /input formats them and drops them in /output

inputFile = open('input/products-gr.txt', 'r')
outputFile = open('output/products.txt', 'a')
lineCount = 0

outputFile.truncate(0)

while True: 
	lineCount += 1
	line = inputFile.readline().strip()

	if not line:
		break

	link = line.split('/')
	title = link[-1][:-5]
	
	t = '-'.join(title.split())
	u = ''.join([c for c in title if c.isalnum() or c=='-'])

	text = u[:20].rstrip('-').lower()
	final = f'{link[0]}//{link[2]}/{text}.html\n'
	
	print(final.strip())

	outputFile.write(final)

outputFile.close()
inputFile.close()